package kdvn.sky2.settings.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.entity.VillagerReplenishTradeEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerAdvancementDoneEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import kdvn.sky2.settings.config.SSConfigValue;
import kdvn.sky2.settings.main.MainSettings;
import kdvn.sky2.settings.utils.Utils;

public class SSListener implements Listener {
	
	@EventHandler
	public void onVillagerTrade(VillagerReplenishTradeEvent e) {
		System.out.println("Replenish");
	}
	
	@EventHandler
	public void onVillagerTrade(InventoryOpenEvent e) {
		if (!SSConfigValue.VILLAGER_TRADE && e.getInventory().getType() == InventoryType.MERCHANT) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerHealthChange(EntityRegainHealthEvent e) {
		Utils.changeHealthScale(e);
	}
	
	@EventHandler
	public void onPlayerHealthChange2(PlayerLoginEvent e) {
		Utils.changeHealthScale2(e);
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {
		if (SSConfigValue.KEEP_ALL_WORLDS) {
			Utils.keepAllWorlds(e);
		}
		if (SSConfigValue.KEEP_LEVEL) {
			Utils.keepLevel(e);
		}
	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e) {
		if (SSConfigValue.DISABLE_FLY_WORLDS.contains(e.getPlayer().getWorld().getName())) {
			Utils.disableFly(e);
		}
	}
	
	@EventHandler
	public void onPlayerCommand(PlayerCommandPreprocessEvent e) {
		Utils.disableCommands(e);
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		if (SSConfigValue.DISABLE_ATTACK_SPEED) {
			Utils.disableAttackSpeed(e);
		}
	}
	
	@EventHandler
	public void onProjectileLaunch(ProjectileLaunchEvent e) {
		if (SSConfigValue.DISABLE_LEFT_HAND_BOW) {
			Utils.disableLeftHandBow(e);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onExp(PlayerExpChangeEvent e) {
		if (SSConfigValue.DISABLE_VANILLA_EXP) {
			Utils.disableVanillaExp(e);
		}
	}
	
	@EventHandler
	public void onExp2(PlayerAdvancementDoneEvent e) { 
		if (SSConfigValue.DISABLE_VANILLA_EXP) {
			Player player = e.getPlayer();
			int level = player.getLevel();
			float exp = player.getExp();
			if (exp < 0 || exp > 1) {
				System.out.println("[Settings] ERROR " + player.getName() + " HAS WRONG EXP");
				System.out.println("[Settings] Achiement: " + e.getAdvancement().toString());
			}
			Bukkit.getScheduler().runTaskLater(MainSettings.getMain(), () -> {
				player.setExp(Math.min(Math.max(0, exp), 1));
				player.setLevel(level);
			}, 5);
		}
	}
	
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		if (SSConfigValue.PLAY_BLOOD_EFFECT) {
			if (e.getDamager() instanceof Player) {
				Player player = (Player) e.getDamager();
				Utils.playBloodEffect(player, e.getEntity());
			}
		}
	}
	
	@EventHandler
	public void onCraft(PrepareItemCraftEvent e) {
		if (SSConfigValue.DISABLE_HOPPER) {
			Utils.disableHopper1(e);
		}
	}
	
	@EventHandler
	public void onInvPickupItem(InventoryPickupItemEvent e) {
		if (SSConfigValue.DISABLE_HOPPER) {
			Utils.disableHopper2(e);
		}
	}
	
	@EventHandler
	public void onEntityExplode(EntityExplodeEvent e) {
		if (SSConfigValue.DISABLE_BLOCK_BREAK_EXPLOSION) {
			Utils.disableExplosion(e);
		}
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if (SSConfigValue.PLAYER_LAVA_DIE_INSTANT.contains(e.getEntity().getWorld().getName())) {
			Utils.lavaDieInstant(e);
		}
	}
	
	@EventHandler
	public void onDamage(PlayerTeleportEvent e) {
		if (e.getPlayer().hasMetadata("NPC")) return;
		if (SSConfigValue.WORLD_TELEPORT_PERMS.containsKey(e.getTo().getWorld().getName())) {
			String perm = SSConfigValue.WORLD_TELEPORT_PERMS.get(e.getTo().getWorld().getName());
			if (!e.getPlayer().hasPermission(perm)) Utils.cancelTeleport(e);
		}
	}
}
