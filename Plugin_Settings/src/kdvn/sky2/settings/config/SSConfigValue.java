package kdvn.sky2.settings.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.configuration.file.FileConfiguration;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class SSConfigValue {
	
	public static int FIXED_HEALTH_SCALE = 20;
	public static boolean KEEP_ALL_WORLDS = true;
	public static boolean DISABLE_PARTICLE_DAMAGEINDICATOR = true;
	public static List<String> DISABLE_FLY_WORLDS = new ArrayList<String> ();
	public static List<String> DISABLE_COMMANDS = new ArrayList<String> ();
	public static boolean DISABLE_ATTACK_SPEED = true;
	public static boolean PLAY_BLOOD_EFFECT = true;
	public static boolean DISABLE_LEFT_HAND_BOW = true;
	public static boolean DISABLE_VANILLA_EXP = true;
	public static boolean DISABLE_HOPPER = true;
	public static boolean DISABLE_BLOCK_BREAK_EXPLOSION = true;
	public static List<String> PLAYER_LAVA_DIE_INSTANT = Lists.newArrayList();
	public static Map<String, String> WORLD_TELEPORT_PERMS = Maps.newHashMap();
	public static boolean KEEP_LEVEL = true;
	public static boolean ALLOW_TNT = true;
	public static boolean VILLAGER_TRADE = true;
	
	public static void load(FileConfiguration config) {
		FIXED_HEALTH_SCALE = config.getInt("fixed-health-scale");
		KEEP_ALL_WORLDS = config.getBoolean("keep-all-worlds");
		DISABLE_PARTICLE_DAMAGEINDICATOR = config.getBoolean("disable-particle-damageindicator");
		DISABLE_FLY_WORLDS = config.getStringList("disable-fly-worlds"); 
		DISABLE_COMMANDS = config.getStringList("disable-commands");
		DISABLE_ATTACK_SPEED = config.getBoolean("disable-attack-speed");
		PLAY_BLOOD_EFFECT = config.getBoolean("play-blood-effect");
		DISABLE_LEFT_HAND_BOW = config.getBoolean("disable-left-hand-bow");
		DISABLE_VANILLA_EXP = config.getBoolean("disable-vanilla-exp");
		DISABLE_HOPPER = config.getBoolean("disable-hopper");
		DISABLE_BLOCK_BREAK_EXPLOSION = config.getBoolean("disable-block-break-explosion");
		PLAYER_LAVA_DIE_INSTANT = config.getStringList("player-lava-die-instant");
		WORLD_TELEPORT_PERMS.clear();
		config.getStringList("world-teleport-perm").forEach(s -> {
			WORLD_TELEPORT_PERMS.put(s.split(":")[0], s.split(":")[1]);
		});
		KEEP_LEVEL = config.getBoolean("keep-level");
		
		if (!config.contains("allow-tnt")) {
			config.set("allow-tnt", true);
		}
		ALLOW_TNT = config.getBoolean("allow-tnt");
		VILLAGER_TRADE = config.getBoolean("villager-trade");
	}
	
}
