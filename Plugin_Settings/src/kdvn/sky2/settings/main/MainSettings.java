package kdvn.sky2.settings.main;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import kdvn.sky2.settings.command.AdminCommand;
import kdvn.sky2.settings.config.Configs;
import kdvn.sky2.settings.listener.EventListener;
import kdvn.sky2.settings.utils.Utils;

public class MainSettings extends JavaPlugin {
	
	private static MainSettings main;
	public static MainSettings getMain() {return main;}
	
	private FileConfiguration config;
	
	@Override
	public void onEnable() {
		main = this;
		this.saveDefaultConfig();
		this.reloadConfig();
		
		// Listener
		Bukkit.getPluginManager().registerEvents(new EventListener(), this);
		
		// 
		if (Configs.DISABLE_PARTICLE_DAMAGEINDICATOR) {
			Utils.disableParticleDamageIndicator(this);
		}
		
		// Command
		this.getCommand("settings").setExecutor(new AdminCommand());
	}
	
	public void reloadConfig() {
		File file = new File(this.getDataFolder(), "config.yml");
		config = YamlConfiguration.loadConfiguration(file);
		Configs.load(config);
	}
	
	public FileConfiguration getConfig() {
		return this.config;
	}
	
}
