package kdvn.sky2.settings.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import kdvn.sky2.settings.main.MainSettings;

public class AdminCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg, String[] args) {
		
		if (!sender.hasPermission("ss.*")) {
			sender.sendMessage("§aKhông đủ đẳng cấp!");
			return false;
		}
		
		if (args.length != 0 && args[0].equalsIgnoreCase("reload")) {
			MainSettings.getMain().reloadConfig();
			sender.sendMessage("Reloaded");
		}
		
		return false;
	}

}
