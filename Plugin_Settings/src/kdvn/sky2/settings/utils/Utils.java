package kdvn.sky2.settings.utils;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.LingeringPotionSplashEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.Plugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.google.common.collect.Lists;

import kdvn.sky2.settings.config.Configs;
import kdvn.sky2.settings.main.MainSettings;


public class Utils {
	
	public static void changeHealthScale(EntityRegainHealthEvent e) {
		if (e.getEntity() instanceof Player) {
			Player player = (Player) e.getEntity();
			player.setHealthScale(Configs.FIXED_HEALTH_SCALE);
		}
	}
	
	public static void changeHealthScale2(PlayerLoginEvent e) {
		Player player = e.getPlayer();
		player.setHealthScale(Configs.FIXED_HEALTH_SCALE);
	}
	
	public static void keepAllWorlds(PlayerDeathEvent e) {
		e.setKeepInventory(true);
		e.setKeepLevel(true);
	}
	
	public static void disableParticleDamageIndicator(Plugin plugin) {
		ProtocolManager pM = ProtocolLibrary.getProtocolManager();
		 pM.addPacketListener(new PacketAdapter(plugin, ListenerPriority.NORMAL, PacketType.Play.Server.WORLD_PARTICLES) {
			@Override
			    public void onPacketSending(PacketEvent e) {
					e.getPacket().getParticles().getValues().forEach(w -> {
						if (w == EnumWrappers.Particle.DAMAGE_INDICATOR) e.setCancelled(true);
					});
			    }
		});
	}
	
	public static void disableFly(PlayerMoveEvent e) {
		Player player = e.getPlayer();
		if (player.hasPermission("sky2.*")) return;
		if (player.isFlying()) {
			e.setCancelled(true);
			player.setFlying(false);
			player.sendMessage("§cChỗ này không cho phép bay, ra bar rồi bay");
		}
	}
	
	public static void disableCommands(PlayerCommandPreprocessEvent e) {
		Player player = e.getPlayer();
		if (player.hasPermission("sky2.*")) return;
		String cmd = e.getMessage().replace("/", "");
		if (Configs.DISABLE_COMMANDS.contains(cmd)) {
			e.setCancelled(true);
			player.sendMessage("§cKhông thể xài lệnh này");
		}
	}
	
	public static void disableAttackSpeed(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		if (player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).getBaseValue() != 16) {
			player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(16);
		}	
	}
	
	public static void playBloodEffect(Player player, Entity entity) {
		if (entity.hasMetadata("NPC")) return;
		player.spawnParticle(Particle.BLOCK_DUST, entity.getLocation().clone().add(0, 1, 0), 20, 0.1, 0.1, 0.1, 0.1, new MaterialData(Material.REDSTONE_BLOCK));
	}
	
	public static void disableLeftHandBow(ProjectileLaunchEvent e) {
		if (e.getEntity().getShooter() instanceof Player) {
			Player player = (Player) e.getEntity().getShooter();
			if (player.getInventory().getItemInOffHand() != null) {
				if (player.getInventory().getItemInOffHand().getType() == Material.BOW && e.getEntity().getType() == EntityType.ARROW) {
					e.setCancelled(true);
					player.sendMessage("§cKhông thể bắn cung khi cầm bằng tay phụ");
				}
			}
		}
	}
	
	public static void disableVanillaExp(PlayerExpChangeEvent e) {
		e.setAmount(0);
	}
	
	public static void disableHopper1(PrepareItemCraftEvent e) {
		if (e.getRecipe() == null) return;
		if (e.getRecipe().getResult() == null) return;
		Material type = e.getRecipe().getResult().getType();
		if (type == Material.HOPPER || type == Material.HOPPER_MINECART) {
			e.getInventory().setResult(new ItemStack(Material.AIR));
			for(HumanEntity he : e.getViewers()) {
                if(he instanceof Player) {
                    ((Player) he).sendMessage("§cKhông thể craft Hopper");
                    ((Player) he).sendMessage("§cĐã có tính năng /kho thay thế!");
                }
            }
		}	
	}
	
	public static void disableHopper2(InventoryPickupItemEvent e) {
		Inventory inv = e.getInventory();
		if (inv.getType() == InventoryType.HOPPER) {
			e.setCancelled(true);
		} 
	}
	
	public static void disableExplosion(EntityExplodeEvent e) {
		if (e.getEntityType() == EntityType.PRIMED_TNT) {
			if (Configs.ALLOW_TNT) return;
		}
		e.blockList().clear();
	}	
	
	public static void lavaDieInstant(EntityDamageEvent e) {
		if (e.getCause() == DamageCause.LAVA && e.getEntity() instanceof Player) {
			Player player = (Player) e.getEntity();
			if (player.hasPermission("ss.*")) return;
			Bukkit.getScheduler().runTask(MainSettings.getMain(), () -> {
				player.setHealth(0);
			});
		}
	}
	
	public static void cancelTeleport(PlayerTeleportEvent e) {
		e.setCancelled(true);
		e.getPlayer().sendMessage("§cHiện tại chưa thể dịch chuyển tới đây");
	}
	
	public static void keepLevel(PlayerDeathEvent e) {
		e.setKeepLevel(true);
		e.setDroppedExp(0);
	}
	
	public static void disablePotion1(PlayerItemConsumeEvent e) {
		Player player = e.getPlayer();
		if (!Configs.DISABLE_POTION_WORLDS.contains(player.getWorld().getName()) || player.hasPermission("ss.*")) return;
		ItemStack is = e.getItem();
		if (is.getType() == Material.POTION || is.getType() == Material.SPLASH_POTION || is.getType() == Material.LINGERING_POTION) {
			e.setCancelled(true);
			player.sendMessage("§cKhông thể dùng item này ở đây");
		}
	}
	
	public static void disablePotion2(PlayerTeleportEvent e) {
		Bukkit.getScheduler().runTaskLater(MainSettings.getMain(), () -> {
			Player player = e.getPlayer();
			if (!Configs.DISABLE_POTION_WORLDS.contains(player.getWorld().getName()) || player.hasPermission("ss.*")) return;
			Lists.newArrayList(player.getActivePotionEffects()).forEach(p -> {
				player.removePotionEffect(p.getType());
			});
		}, 5);
	}
	
	public static void disablePotion3(LingeringPotionSplashEvent e) {
		if (!Configs.DISABLE_POTION_WORLDS.contains(e.getEntity().getWorld().getName())) return;
		e.setCancelled(true);

	}
	
}
